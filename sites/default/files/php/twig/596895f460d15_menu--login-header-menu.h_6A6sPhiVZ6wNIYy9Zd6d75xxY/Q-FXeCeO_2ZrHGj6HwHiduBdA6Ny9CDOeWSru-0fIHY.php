<?php

/* themes/custom/iitb/templates/menu--login-header-menu.html.twig */
class __TwigTemplate_a18b5f26b950a1732342ae84facd2c074daa153f68be57367a13e28600094086 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("import" => 58, "macro" => 67, "if" => 69, "for" => 93);
        $filters = array("t" => 89);
        $functions = array("url" => 76, "path" => 89, "link" => 96);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('import', 'macro', 'if', 'for'),
                array('t'),
                array('url', 'path', 'link')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 57
        echo "
";
        // line 58
        $context["menus"] = $this;
        // line 59
        echo "
";
        // line 64
        echo "
";
        // line 65
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links(($context["items"] ?? null), ($context["attributes"] ?? null), 0, ($context["edx_site_path"] ?? null), ($context["logged_username"] ?? null), ($context["logged_email"] ?? null), ($context["profile_link"] ?? null), ($context["logout_link"] ?? null), ($context["account_link"] ?? null))));
        echo "

";
        // line 134
        echo "
<div id=\"after_login_menu\" style=\"display:none;\">
<ol class=\"user\" id=\"login_menu_one\">
  <li class=\"primary\">
     <a class=\"user-link\" href=\"";
        // line 138
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
        echo "/dashboard\">
          <span class=\"sr\">Dashboard for:</span>
          <div id='username'> ";
        // line 140
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["logged_username"] ?? null), "html", null, true));
        echo " varunmadkaikar
          </div>
          </a>
      </li>
      <li class=\"primary\">
        <button class=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\" id=\"test123\"><span class=\"sr\">More options dropdown</span><span class=\"fa fa-sort-desc\" aria-hidden=\"true\"></span></button>
       <ul class='dropdown-menu' aria-label='More Options' role='menu'>
        <li><a href=\"";
        // line 147
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
        echo "/dashboard\">Dashboard</a></li>
          <li><a href=\"";
        // line 148
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["account_link"] ?? null), "html", null, true));
        echo "\" id=\"account_link\">Account</a></li>
 <li><a href=\"";
        // line 149
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["profile_link"] ?? null), "html", null, true));
        echo "\" id=\"profile_link\">Profile</a></li>
<li><a href=\"";
        // line 150
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["logout_link"] ?? null), "html", null, true));
        echo "\" role='menuitem' id=\"logout_link\">Logout</a></li>

       </ul>
           </li>
    </ol>




<ol class=\"left list-inline nav-global\" id=\"login_menu_two\">
  <li class=\"nav-global-01\">
      <a href=\"";
        // line 161
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
        echo "/dashboard\">Dashboard</a>
  </li>
  <li class=\"nav-global-01\">
      <a href=\"";
        // line 164
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["account_link"] ?? null), "html", null, true));
        echo "\" id=\"account_link_1\">Account</a>
  </li>
  <li class=\"nav-global-01\">
      <a href=\"";
        // line 167
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["profile_link"] ?? null), "html", null, true));
        echo "\" id=\"profile_link_1\">Profile</a>
  </li>
  <li class=\"nav-global-01\">
      <a href=\"";
        // line 170
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["logout_link"] ?? null), "html", null, true));
        echo "\" role='menuitem' id=\"logout_link_1\">Logout</a>
  </li>
</ol>
</div>
";
    }

    // line 67
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, $__edx_site_path__ = null, $__logged_username__ = null, $__logged_email__ = null, $__profile_link__ = null, $__logout_link__ = null, $__account_link__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "edx_site_path" => $__edx_site_path__,
            "logged_username" => $__logged_username__,
            "logged_email" => $__logged_email__,
            "profile_link" => $__profile_link__,
            "logout_link" => $__logout_link__,
            "account_link" => $__account_link__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 68
            echo "  ";
            $context["menus"] = $this;
            // line 69
            echo "  ";
            if (($context["items"] ?? null)) {
                // line 70
                echo "

    <ol class=\"left list-inline nav-global\" id=\"mobile_menu\" >

                <li class=\"list-inline nav-global-01\" id=\"header_menu_one\" >
                 
                     <form method=\"get\" action=\"";
                // line 76
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>")));
                echo "/courses\" >
                                          <section class=\"wrapper_special course-search\">

                                              <div style=\"float:left;width:100%;\"><input name=\"search_query\" id=\"search_key_two\" type=\"text\" placeholder=\"Courses, Faculty, Domain\"></input></div>
                                              <div style=\"float:left;width:0%;\"><button type=\"submit\" id=\"search_course\">
                                              <span class=\"icon fa fa-search\" aria-hidden=\"true\"></span>
                                              </button></div>
                                          </section>
                                      </form>
                   
                      
                </li>
             <li id=\"home_icon\" class=\"nav-global-01\">
                <a href=\"";
                // line 89
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("<front>")));
                echo "\" title=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Home")));
                echo "\" rel=\"home\"><div class=\"home_icon\"></div></a>
          </li>
 

        ";
                // line 93
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 94
                    echo "
        <li class=\"nav-global-01\">
\t\t";
                    // line 96
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->getAttribute($context["item"], "title", array()), $this->getAttribute($context["item"], "url", array())), "html", null, true));
                    echo "
\t\t";
                    // line 97
                    if ($this->getAttribute($context["item"], "below", array())) {
                        // line 98
                        echo "\t\t\t";
                        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", array()), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1))));
                        echo "
\t\t";
                    }
                    // line 100
                    echo "
    </li>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 103
                echo "\t
\t\t\t<li class=\"list-inline nav-global-04\" id=\"header_menu_two\" style=\"margin-left:17px;\">
\t\t\t\t<div class=\"title\">
\t\t\t\t\t<div class=\"course-search\">
\t\t\t       \t\t\t<form method=\"get\" action=\"";
                // line 107
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>")));
                echo "/courses\">
                                       \t\t\t<input class=\"search-input\" name=\"search_query\" id=\"search_key_one\" type=\"text\" placeholder=\"Courses, Faculty, Domain\"></input>
                                        \t\t\t<button class=\"search-button\" type=\"submit\" id=\"search_course\">
                                        \t\t\t<span class=\"icon fa fa-search\" aria-hidden=\"true\"></span>
                                        \t\t\t</button>

                                \t\t\t</form>
                        \t\t\t</div>
                \t\t\t</div>
        \t\t\t</li>


\t\t
         
          
        
    </ol>
    <div id=\"login_tab\" style=\"display:block;\">
    <ol class=\"right nav-courseware nav navbar-nav navbar-right\">
            <li class=\"nav-courseware-01\">
              <a class=\"cta cta-login\" href=\"";
                // line 127
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
                echo "/login\">Login</a>
            </li>
       </ol>
    </div>

  ";
            }
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/custom/iitb/templates/menu--login-header-menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 127,  226 => 107,  220 => 103,  212 => 100,  206 => 98,  204 => 97,  200 => 96,  196 => 94,  192 => 93,  183 => 89,  167 => 76,  159 => 70,  156 => 69,  153 => 68,  133 => 67,  124 => 170,  118 => 167,  112 => 164,  106 => 161,  92 => 150,  88 => 149,  84 => 148,  80 => 147,  70 => 140,  65 => 138,  59 => 134,  54 => 65,  51 => 64,  48 => 59,  46 => 58,  43 => 57,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/iitb/templates/menu--login-header-menu.html.twig", "/var/www/html/iitbx/themes/custom/iitb/templates/menu--login-header-menu.html.twig");
    }
}
