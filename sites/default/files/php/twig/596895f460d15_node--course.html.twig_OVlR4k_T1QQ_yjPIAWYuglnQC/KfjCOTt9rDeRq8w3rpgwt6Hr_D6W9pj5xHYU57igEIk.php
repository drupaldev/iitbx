<?php

/* themes/custom/iitb/templates/node--course.html.twig */
class __TwigTemplate_fedae53c26f41128078d9bd20e125f24ffa3346dc74ae0a377e6bc8099a6ba3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 39, "for" => 55, "if" => 59, "trans" => 139);
        $filters = array("clean_class" => 41, "trim" => 59, "upper" => 110, "slice" => 110, "date" => 139);
        $functions = array("attach_library" => 49, "url" => 121);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'for', 'if', 'trans'),
                array('clean_class', 'trim', 'upper', 'slice', 'date'),
                array('attach_library', 'url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 39
        $context["classes"] = array(0 => "node", 1 => ("node--type-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 41
($context["node"] ?? null), "bundle", array()))), 2 => (($this->getAttribute(        // line 42
($context["node"] ?? null), "isPromoted", array(), "method")) ? ("node--promoted") : ("")), 3 => (($this->getAttribute(        // line 43
($context["node"] ?? null), "isSticky", array(), "method")) ? ("node--sticky") : ("")), 4 => (( !$this->getAttribute(        // line 44
($context["node"] ?? null), "isPublished", array(), "method")) ? ("node--unpublished") : ("")), 5 => ((        // line 45
($context["view_mode"] ?? null)) ? (("node--view-mode-" . \Drupal\Component\Utility\Html::getClass(($context["view_mode"] ?? null)))) : ("")));
        // line 48
        echo "
";
        // line 49
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("classy/node"), "html", null, true));
        echo "
<article ";
        // line 50
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo ">
  <div ";
        // line 51
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["content_attributes"] ?? null), "addClass", array(0 => "node__content"), "method"), "html", null, true));
        echo ">
       ";
        // line 52
        $context["fcount"] = 18;
        echo "  
       ";
        // line 53
        $context["course_code"] = "";
        // line 54
        echo "       ";
        $context["course_timeline"] = "";
        // line 55
        echo "       ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["content"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["row"]) {
            echo "            

             ";
            // line 57
            $context["fcount"] = (($context["fcount"] ?? null) - 1);
            // line 58
            echo "                           
             ";
            // line 59
            if ((twig_trim_filter($context["key"]) == "field_course_code")) {
                // line 60
                echo "             ";
                $context["course_code"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 61
                echo "             ";
            }
            echo "   

             ";
            // line 63
            if ((twig_trim_filter($context["key"]) == "field_course_description")) {
                echo " 
             ";
                // line 64
                $context["course_description"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 65
                echo "             ";
            }
            // line 66
            echo "
             ";
            // line 67
            if ((twig_trim_filter($context["key"]) == "field_course_link")) {
                // line 68
                echo "             ";
                $context["course_link"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 69
                echo "             ";
            }
            // line 70
            echo "
             ";
            // line 71
            if ((twig_trim_filter($context["key"]) == "field_course_end")) {
                echo " 
             ";
                // line 72
                $context["end_date"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 73
                echo "             ";
            }
            // line 74
            echo "
             ";
            // line 75
            if ((twig_trim_filter($context["key"]) == "field_course_image")) {
                echo " 
             ";
                // line 76
                $context["image_link"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 77
                echo "             ";
            }
            // line 78
            echo "
             ";
            // line 79
            if ((twig_trim_filter($context["key"]) == "field_is_new_course")) {
                // line 80
                echo "             ";
                $context["new_course"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 81
                echo "             ";
            }
            // line 82
            echo "
             ";
            // line 83
            if ((twig_trim_filter($context["key"]) == "field_course_announcement")) {
                // line 84
                echo "             ";
            }
            // line 85
            echo "
             ";
            // line 86
            if ((twig_trim_filter($context["key"]) == "field_course_start")) {
                echo " 
             ";
                // line 87
                $context["start_date"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 88
                echo "             ";
            }
            // line 89
            echo "
             ";
            // line 90
            if ((twig_trim_filter($context["key"]) == "field_course_subjects")) {
                echo " 
             ";
                // line 91
                $context["subject"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 92
                echo "             ";
            }
            // line 93
            echo "
             ";
            // line 94
            if ((twig_trim_filter($context["key"]) == "field_course_name")) {
                echo " 
             ";
                // line 95
                $context["course_name"] = $this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), "value", array());
                // line 96
                echo "             ";
            }
            // line 97
            echo "
             ";
            // line 98
            if ((twig_trim_filter($context["key"]) == "field_time_line")) {
                // line 99
                echo "             ";
                $context["course_timeline1"] = $this->getAttribute($this->getAttribute($this->getAttribute($context["row"], "#items", array(), "array"), 0, array(), "array"), "value", array());
                // line 100
                echo "             ";
            }
            // line 101
            echo "  ";
            // line 105
            echo "           
             ";
            // line 106
            if ((twig_trim_filter($context["key"]) == "field_domains")) {
                // line 107
                echo "             ";
                ob_start();
                // line 108
                echo "        \t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["row"], "#items", array(), "array"));
                foreach ($context['_seq'] as $context["keyx"] => $context["valuex"]) {
                    // line 109
                    echo "\t              \t     <div class=\"domain_div\">
                             \t";
                    // line 110
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_upper_filter($this->env, twig_slice($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($context["valuex"], "entity", array()), "field_domain_name", array()), 0, array()), "value", array()), 0, 2)), "html", null, true));
                    echo "
                             </div>
\t         \t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['keyx'], $context['valuex'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 113
                echo "             ";
                $context["domain_string"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
                // line 114
                echo "             ";
            }
            // line 115
            echo "                  
             
             ";
            // line 117
            if ((($context["fcount"] ?? null) == 0)) {
                // line 118
                echo "              <ul class=\"courses-listing\" >
              <li class=\"courses-listing-item\">
               <article class=\"course\" id=\"";
                // line 120
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_link"] ?? null), "html", null, true));
                echo "\" role=\"region\" aria-label=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_name"] ?? null), "html", null, true));
                echo "\">
                <a href=\"";
                // line 121
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<front>")));
                echo "course/about/";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["node"] ?? null), "id", array()), "html", null, true));
                echo "\">
                  <header class=\"course-image\">
                    <div class=\"cover-image\">

                    ";
                // line 125
                if ((($context["new_course"] ?? null) == 1)) {
                    // line 126
                    echo "                        <span class=\"status\">New</span>";
                }
                // line 128
                echo "
                    <img src=\"";
                // line 129
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
                echo "/";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["image_link"] ?? null), "html", null, true));
                echo "\" alt=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_code"] ?? null), "html", null, true));
                echo " - ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_code"] ?? null), "html", null, true));
                echo "\">
                    <div class=\"learn-more\" aria-hidden=\"true\">LEARN MORE</div>
                    </div>
                  </header>
                 <section class=\"course-info\" aria-hidden=\"true\">
                    <h2 class=\"course-name\">
                      <span class=\"course-code\">";
                // line 135
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_code"] ?? null), "html", null, true));
                echo "</span>
                      <span class=\"course-title\">";
                // line 136
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_name"] ?? null), "html", null, true));
                echo "</span>
                    </h2>
                    <div class=\"course-status\" aria-hidden=\"true\">";
                // line 138
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_timeline1"] ?? null), "html", null, true));
                echo " </div>
                   <div class=\"course-date\" aria-hidden=\"true\">";
                // line 139
                echo t("Starts:", array());
                echo " ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, ($context["start_date"] ?? null), "M d, Y"), "html", null, true));
                echo "</div>
                   ";
                // line 140
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["domain_string"] ?? null), "html", null, true));
                echo "
                   <div>
                  </div>
                 </section>
                 ";
                // line 151
                echo "                </a>
              </article>

              </li>
             </ul>
            ";
            }
            // line 157
            echo "       ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 158
        echo "  </div>
</article>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/iitb/templates/node--course.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  329 => 158,  323 => 157,  315 => 151,  308 => 140,  302 => 139,  298 => 138,  293 => 136,  289 => 135,  274 => 129,  271 => 128,  268 => 126,  266 => 125,  257 => 121,  251 => 120,  247 => 118,  245 => 117,  241 => 115,  238 => 114,  235 => 113,  226 => 110,  223 => 109,  218 => 108,  215 => 107,  213 => 106,  210 => 105,  208 => 101,  205 => 100,  202 => 99,  200 => 98,  197 => 97,  194 => 96,  192 => 95,  188 => 94,  185 => 93,  182 => 92,  180 => 91,  176 => 90,  173 => 89,  170 => 88,  168 => 87,  164 => 86,  161 => 85,  158 => 84,  156 => 83,  153 => 82,  150 => 81,  147 => 80,  145 => 79,  142 => 78,  139 => 77,  137 => 76,  133 => 75,  130 => 74,  127 => 73,  125 => 72,  121 => 71,  118 => 70,  115 => 69,  112 => 68,  110 => 67,  107 => 66,  104 => 65,  102 => 64,  98 => 63,  92 => 61,  89 => 60,  87 => 59,  84 => 58,  82 => 57,  74 => 55,  71 => 54,  69 => 53,  65 => 52,  61 => 51,  57 => 50,  53 => 49,  50 => 48,  48 => 45,  47 => 44,  46 => 43,  45 => 42,  44 => 41,  43 => 39,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/iitb/templates/node--course.html.twig", "/var/www/html/iitbx/themes/custom/iitb/templates/node--course.html.twig");
    }
}
