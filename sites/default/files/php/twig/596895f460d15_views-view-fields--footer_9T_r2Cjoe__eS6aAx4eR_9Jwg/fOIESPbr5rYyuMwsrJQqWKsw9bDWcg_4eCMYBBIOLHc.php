<?php

/* themes/custom/iitb/templates/views-view-fields--footer_logo.html.twig */
class __TwigTemplate_d0fcdeb16ca87caf7bdab68fcd44f05cd029ed96973f8b9bbdcebfd56ee06863 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 38, "for" => 40, "if" => 44);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'for', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 36
        echo "

";
        // line 38
        list($context["icon_link"], $context["icon_image"]) =         array("", "");
        // line 39
        $context["fcount"] = 2;
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 42
            $context["fcount"] = (($context["fcount"] ?? null) - 1);
            // line 44
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-link")) {
                // line 45
                echo "
                ";
                // line 46
                $context["icon_link"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 51
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-symbol")) {
                // line 52
                echo "
                ";
                // line 53
                $context["icon_image"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 56
            echo "

    ";
            // line 58
            if ((($context["fcount"] ?? null) == 0)) {
                // line 59
                echo "
           <a href=\"";
                // line 60
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["icon_link"] ?? null), "html", null, true));
                echo "\" ><img src=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["icon_image"] ?? null), "html", null, true));
                echo "\" /></a>";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "themes/custom/iitb/templates/views-view-fields--footer_logo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 60,  79 => 59,  77 => 58,  73 => 56,  70 => 53,  67 => 52,  65 => 51,  62 => 46,  59 => 45,  57 => 44,  55 => 42,  51 => 40,  49 => 39,  47 => 38,  43 => 36,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/iitb/templates/views-view-fields--footer_logo.html.twig", "/var/www/html/iitbx/themes/custom/iitb/templates/views-view-fields--footer_logo.html.twig");
    }
}
