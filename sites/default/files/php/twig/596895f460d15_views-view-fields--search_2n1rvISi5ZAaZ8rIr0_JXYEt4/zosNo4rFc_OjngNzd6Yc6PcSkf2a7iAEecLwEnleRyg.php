<?php

/* themes/custom/iitb/templates/views-view-fields--search_course_content.html.twig */
class __TwigTemplate_3787f1b8d9a87daaf56e5745c6b57ab02bd122205931051afda59c85a941db82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 38, "for" => 41, "if" => 46);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'for', 'if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 37
        echo "
";
        // line 38
        list($context["title"], $context["desc"]) =         array("", "");
        // line 39
        $context["fcount"] = 2;
        // line 40
        echo "
";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 43
            $context["fcount"] = (($context["fcount"] ?? null) - 1);
            // line 46
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-title")) {
                // line 47
                echo "
                ";
                // line 48
                $context["title"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 53
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-body")) {
                // line 54
                echo "
                ";
                // line 55
                $context["desc"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 58
            echo "         

    ";
            // line 60
            if ((($context["fcount"] ?? null) == 0)) {
                // line 61
                echo "
                <div>
\t\t\t<h2 class=\"headline\">";
                // line 63
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
                echo "</h2>
\t\t\t<div class=\"content\">";
                // line 64
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["desc"] ?? null), "html", null, true));
                echo "</div>
\t\t</div>";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "themes/custom/iitb/templates/views-view-fields--search_course_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 64,  85 => 63,  81 => 61,  79 => 60,  75 => 58,  72 => 55,  69 => 54,  67 => 53,  64 => 48,  61 => 47,  59 => 46,  57 => 43,  53 => 41,  50 => 40,  48 => 39,  46 => 38,  43 => 37,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/iitb/templates/views-view-fields--search_course_content.html.twig", "/var/www/html/iitbx/themes/custom/iitb/templates/views-view-fields--search_course_content.html.twig");
    }
}
