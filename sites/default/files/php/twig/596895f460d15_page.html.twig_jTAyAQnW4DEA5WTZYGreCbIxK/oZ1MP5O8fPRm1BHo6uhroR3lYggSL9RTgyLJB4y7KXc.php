<?php

/* themes/custom/iitb/templates/page.html.twig */
class __TwigTemplate_eae6b392d9c38594a6aa4d146669ed9938f9053d792361271656be10ac5e6d2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 49, "if" => 50);
        $filters = array("raw" => 88);
        $functions = array("file_url" => 84, "url" => 143);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if'),
                array('raw'),
                array('file_url', 'url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 48
        echo "
";
        // line 49
        $context["class_var"] = "index_page";
        // line 50
        if (($context["node"] ?? null)) {
            // line 51
            echo "  ";
            if ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_node_class", array()), "value", array())) {
                // line 52
                echo "  ";
                $context["class_var"] = $this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_node_class", array()), "value", array());
                // line 53
                echo "  ";
            }
        } elseif (twig_in_filter("/course/about",         // line 54
($context["url"] ?? null))) {
            // line 55
            echo "  ";
            $context["class_var"] = "courses_about_page";
        } elseif (twig_in_filter("/course",         // line 56
($context["url"] ?? null))) {
            // line 57
            echo "  ";
            $context["class_var"] = "course_page";
        }
        // line 59
        echo "

<div class=\"ltr ";
        // line 61
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["class_var"] ?? null), "html", null, true));
        echo " lang_en\">
  <div class=\"window-wrap\" dir=\"ltr\">
     <header id=\"global-navigation\" class=\"header-global\">
       \t<nav class=\"wrapper-header\" aria-label=\"Global\">
      \t     <span data-target=\"#navbarCollapse\" data-toggle=\"collapse\" class=\"navbar-toggle mainmenubutton\"></span>
                <h1 class=\"logo\">
        \t        ";
        // line 67
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header_logo", array()), "html", null, true));
        echo "
       \t        </h1>
\t<div id=\"navbarCollapse\" class=\"collapse navbar-collapse\">
         \t<div id=\"auth_header\" style=\"display:none;\">
\t\t\t";
        // line 71
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "login_header_menu", array()), "html", null, true));
        echo "
   \t\t</div>
\t\t<div id=\"guest_header\" style=\"display:block;\">
\t\t\t";
        // line 74
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "main_menu", array()), "html", null, true));
        echo "\t
\t\t</div>
\t</div> 
       ";
        // line 78
        echo "\t</nav>
     </header>
        <div class=\"main-wrapper\">
           <div class=\"content-wrapper\" id=\"content\">
               <div class=\"outer_container\">
                     ";
        // line 83
        if ((($context["node"] ?? null) && ($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_show_banner", array()), "value", array()) == "1"))) {
            // line 84
            echo "                      <div class=\"slider\"><div class=\"custom_responsive_banner banner_center\" style=\"background-image: url(";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute(($context["node"] ?? null), "field_banner", array()), "entity", array()), "fileuri", array()))), "html", null, true));
            echo ") !important;height:230px\"></div></div>
                     ";
        }
        // line 86
        echo "               <div class=\"main_content\">
                         ";
        // line 87
        if ((($context["node"] ?? null) && (($context["class_var"] ?? null) == "how_it_works"))) {
            echo " 
                         \t";
            // line 88
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["page"] ?? null), "content", array())));
            echo "
                                 <div class='course_mode'> 
\t\t\t\t";
            // line 90
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "course_modes", array()), "html", null, true));
            echo "
                                 </div> 
                         ";
        } elseif ((        // line 92
($context["node"] ?? null) && (($context["class_var"] ?? null) == "contact_us_en"))) {
            // line 93
            echo "        \t                ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["page"] ?? null), "content", array())));
            echo "

                     \t\t<div class=\"row is-flex\">
\t                          <div class=\"col-sm-8 table_column01\">
        \t                 \t<div id=\"contact\">
                \t\t\t    ";
            // line 98
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "contact", array()), "html", null, true));
            echo "
\t\t\t\t\t</div>\t
\t                          </div>
        \t                  <div class=\"col-sm-4 table_column02\">
                \t                <div id=\"contact-email\">
\t\t\t\t\t      ";
            // line 103
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "contact_email", array()), "html", null, true));
            echo "
\t\t\t\t\t</div>
\t                          </div>
        \t\t        </div>
                         ";
        } elseif ((        // line 107
($context["node"] ?? null) && (($context["class_var"] ?? null) == "learners_faq"))) {
            // line 108
            echo "                                ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["page"] ?? null), "content", array())));
            echo "
\t\t\t\t";
            // line 109
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "faq", array()), "html", null, true));
            echo "
                         ";
        } elseif ((        // line 110
($context["node"] ?? null) && ((((($context["class_var"] ?? null) == "research") || (($context["class_var"] ?? null) == "about")) || (($context["class_var"] ?? null) == "privacy_page")) || (($context["class_var"] ?? null) == "tos_page")))) {
            echo "  
                                ";
            // line 111
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["page"] ?? null), "content", array())));
            echo "
\t\t\t ";
        } elseif ((        // line 112
($context["node"] ?? null) && (($context["class_var"] ?? null) == "about team"))) {
            // line 113
            echo "\t\t\t\t";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->getAttribute(($context["page"] ?? null), "content", array())));
            echo "
\t\t\t\t";
            // line 114
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "team", array()), "html", null, true));
            echo " 
                         ";
        } elseif ((        // line 115
($context["node"] ?? null) && (($context["class_var"] ?? null) == "index_page"))) {
            // line 116
            echo "\t                         <div> ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
            echo " </div> 
        \t                 <div class=\"overflowhid\"> ";
            // line 117
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "domains", array()), "html", null, true));
            echo "</div>

                         ";
        } elseif (        // line 119
($context["node"] ?? null)) {
            // line 120
            echo "                                <div style=\"width:100%; text-align:center; height:430px; padding-top:100px; \"><img src=\"themes/custom/iitb/images/coming_soon.jpeg\" /> </div>
                                ";
            // line 121
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
            echo "
                         ";
        } elseif (twig_in_filter("/course/about",         // line 122
($context["url"] ?? null))) {
            // line 123
            echo "                                ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
            echo "


                         ";
        } elseif (twig_in_filter("/course",         // line 126
($context["url"] ?? null))) {
            // line 127
            echo "                                <div class=\"navbar-toggle filtermenubutton\"></div>  
                                <div id=\"content\" class=\"content-wrapper\">
                                  <section class=\"home course_inner_page\">
                                    <section class=\"courses-container\">
                                        <section class=\"highlighted-courses\">
                                          <section class=\"courses\">
 
                                            <div class=\"search_content_one\">
                                                  ";
            // line 135
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "course_search_content", array()), "html", null, true));
            echo "  
                                            </div>
                                            <div class=\"row is-flex\">
                                              <div class=\"col-sm-3 table_column01\" >
                                                <div class=\"coursesidebar\" id=\"navbarCollapsec\" >
                                                  <div class=\"filtertitle\"> Refine Your Search </div>
                                                    ";
            // line 141
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "search_facets", array()), "html", null, true));
            echo "
                                                    <br> 
                                                     <p style=\"float:left\"> <a href=";
            // line 143
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<current>")));
            echo " >Clear All Filters</a>  </p>
                                                  </div>
                                                </div>
                                                <div class=\"col-sm-9 table_column02\">
                                                  <div id=\"search_msg\"></div>
                                                  <section class=\"inner-section\">
                                                    
                                                   ";
            // line 150
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
            echo "
                                                  </section>
                                                </div>
                                              </div>
                                          </section>
                                        </section>
                                    </section>
                                  </section>
                                </div>
                       ";
        } else {
            // line 160
            echo "                           <br>
                           ";
            // line 161
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
            echo "
                       ";
        }
        // line 163
        echo "                    </div>
     \t\t</div>
        </div>
        </div> 

        <div class=\"wrapper wrapper-footer\">
           <footer>
            <div class=\"footer_menu\">
              <nav>  ";
        // line 171
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_menu", array()), "html", null, true));
        echo " </nav>
            </div>
            <div  class=\"legal_menu\">
              <div class=\"legal-menu\"> ";
        // line 174
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_legal", array()), "html", null, true));
        echo " </div>
              <div class=\"open-edx-logo\"> ";
        // line 175
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_logo", array()), "html", null, true));
        echo " </div>
            </div>
          </footer>
        </div>

        <div class=\"wrapper wrapper-footer-two\">
          <footer>
            <div  class=\"copyright\">
               ";
        // line 183
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_copyright", array()), "html", null, true));
        echo "
            </div>
          </footer>
        </div>
  </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "themes/custom/iitb/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  308 => 183,  297 => 175,  293 => 174,  287 => 171,  277 => 163,  272 => 161,  269 => 160,  256 => 150,  246 => 143,  241 => 141,  232 => 135,  222 => 127,  220 => 126,  213 => 123,  211 => 122,  207 => 121,  204 => 120,  202 => 119,  197 => 117,  192 => 116,  190 => 115,  186 => 114,  181 => 113,  179 => 112,  175 => 111,  171 => 110,  167 => 109,  162 => 108,  160 => 107,  153 => 103,  145 => 98,  136 => 93,  134 => 92,  129 => 90,  124 => 88,  120 => 87,  117 => 86,  111 => 84,  109 => 83,  102 => 78,  96 => 74,  90 => 71,  83 => 67,  74 => 61,  70 => 59,  66 => 57,  64 => 56,  61 => 55,  59 => 54,  56 => 53,  53 => 52,  50 => 51,  48 => 50,  46 => 49,  43 => 48,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/iitb/templates/page.html.twig", "/var/www/html/iitbx/themes/custom/iitb/templates/page.html.twig");
    }
}
