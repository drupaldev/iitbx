<?php

/* themes/custom/iitb/templates/views-view-fields--course_about--page.html.twig */
class __TwigTemplate_76623591d57c77d9acb07500f464b4c6599668fe34f88b05f5ecb08fac513d35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 54, "for" => 55, "if" => 58);
        $filters = array("replace" => 156);
        $functions = array("url" => 156);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'for', 'if'),
                array('replace'),
                array('url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 53
        echo "
";
        // line 54
        $context["fcount"] = 10;
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["fields"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 57
            $context["fcount"] = (($context["fcount"] ?? null) - 1);
            // line 58
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-link")) {
                // line 59
                echo "  ";
                $context["course_link"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 62
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-image")) {
                // line 63
                echo "  ";
                $context["image_link"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 66
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-code")) {
                echo " 
  ";
                // line 67
                $context["course_code"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 70
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-name")) {
                // line 71
                echo "  ";
                $context["course_name"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 74
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-display-string")) {
                // line 75
                echo "  ";
                $context["start_date"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 78
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-end")) {
                // line 79
                echo "  ";
                $context["end_date"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 82
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-effort")) {
                // line 83
                echo "  ";
                $context["effort"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 86
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-video")) {
                // line 87
                echo "  ";
                $context["video"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 90
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-enrollment-end")) {
                // line 91
                echo "  ";
                $context["enrollment_end"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 94
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-enrollment-start")) {
                // line 95
                echo "  ";
                $context["enrollment_start"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 97
            echo "
  ";
            // line 98
            if ((($context["fcount"] ?? null) == 0)) {
                // line 99
                echo "     <input type='hidden' value=\"";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_link"] ?? null), "html", null, true));
                echo "\" id=\"course_id\"/>
     <input type='hidden' value=\"";
                // line 100
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
                echo "\" id=\"edx_site_path\"/>
     <input type='hidden' value=\"";
                // line 101
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["enrollment_start"] ?? null), "html", null, true));
                echo "\" id=\"enrollment_start\"/>
     <input type='hidden' value=\"";
                // line 102
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["enrollment_end"] ?? null), "html", null, true));
                echo "\" id=\"enrollment_end\"/>
     <input type='hidden' value=\"";
                // line 103
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_name"] ?? null), "html", null, true));
                echo "\" id=\"course_name\"/>

      <section class=\"course-info\">
  \t<header class=\"course-profile\">
\t    <div class=\"intro-inner-wrapper\">
\t      <div class=\"table\">


             ";
                // line 111
                if ((($context["video"] ?? null) != "")) {
                    // line 112
                    echo "\t       <a class=\"media\" href=\"#video-modal\" rel=\"leanModal\">
\t         <div class=\"hero\">
\t            <img src=\" ";
                    // line 114
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["image_link"] ?? null), "html", null, true));
                    echo "\" alt=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_code"] ?? null), "html", null, true));
                    echo "\" />
\t            <div class=\"play-intro\"></div>
\t\t </div>
               </a>
             ";
                } else {
                    // line 119
                    echo "
              <div class=\"media\">
\t        <div class=\"hero\">
\t           <img src=\" ";
                    // line 122
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["edx_site_path"] ?? null), "html", null, true));
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["image_link"] ?? null), "html", null, true));
                    echo "\" alt=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_code"] ?? null), "html", null, true));
                    echo "\" />
                </div>
              </div>

            ";
                }
                // line 127
                echo "
           <div class=\"intro\">
                <div class=\"heading-group\">
                  <h1> ";
                // line 130
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["site_path"] ?? null), "html", null, true));
                echo "
                    ";
                // line 131
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_name"] ?? null), "html", null, true));
                echo "
                    <a href=\"#\">";
                // line 132
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["organisation"] ?? null), "html", null, true));
                echo "</a>
                  </h1>
                </div>
                <div class=\"main-cta\">
                    <a href=\"#\" class=\"register\">Enroll in ";
                // line 136
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_code"] ?? null), "html", null, true));
                echo " </a>
                     <div id=\"register_error\"/>  </div>
                </div>
               </div>

           </div>
         </div>
      </header>

    <div class=\"container\">
";
                // line 150
                echo "      <div class=\"course-sidebar\">
        <div class=\"course-summary\">
          <header>
            <div class=\"social-sharing\">
            <div class=\"sharing-message\">Share with friends and family!</div>
      
               <a href=\"http://twitter.com/intent/tweet?text=";
                // line 156
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter(($context["mail_body"] ?? null), array("{{course_code}}" => ($context["course_code"] ?? null), "{{course_name}}" => ($context["course_name"] ?? null))), "html", null, true));
                echo " : ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<current>")));
                echo "\" class=\"share\">
                <span class=\"icon fa fa-twitter\" aria-hidden=\"true\"></span>
                <span class=\"sr\">Tweet that you've enrolled in this course</span>
               </a>

               <a href=\"";
                // line 161
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["facebook_path"] ?? null), "html", null, true));
                echo "\" class=\"share\">
                 <span class=\"icon fa fa-thumbs-up\" aria-hidden=\"true\"></span>
                 <span class=\"sr\">Post a Facebook message to say you've enrolled in this course</span>
               </a>
     
               <a href=\"mailto:?subject=";
                // line 166
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["mail_subject"] ?? null), "html", null, true));
                echo "&body= ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter(($context["mail_body"] ?? null), array("{{course_code}}" => ($context["course_code"] ?? null), "{{course_name}}" => ($context["course_name"] ?? null))), "html", null, true));
                echo " : ";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("<current>")));
                echo "\" class=\"share\">
                 <span class=\"icon fa fa-envelope\" aria-hidden=\"true\"></span>
                 <span class=\"sr\">Email someone to say you've enrolled in this course</span>
               </a>

            </div>
           </header>

        <ol class=\"important-dates\">
          ";
                // line 175
                if ((($context["course_code"] ?? null) != "")) {
                    echo "         
             <li class=\"important-dates-item\">
               <span class=\"icon fa fa-info-circle\" aria-hidden=\"true\"></span>
               <p class=\"important-dates-item-title\">Course Number</p>
               <span class=\"important-dates-item-text course-number\">";
                    // line 179
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_code"] ?? null), "html", null, true));
                    echo "</span>
             </li>
          ";
                }
                // line 182
                echo "
          ";
                // line 183
                if ((($context["start_date"] ?? null) != "")) {
                    // line 184
                    echo "            <li class=\"important-dates-item\"><span class=\"icon fa fa-calendar\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Classes Start</p><span class=\"important-dates-item-text start-date\">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["start_date"] ?? null), "html", null, true));
                    echo "</span></li>
          ";
                }
                // line 186
                echo "          ";
                if ((($context["end_date"] ?? null) != "")) {
                    // line 187
                    echo "            <li class=\"important-dates-item\"><span class=\"icon fa fa-calendar\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Classes End</p><span class=\"important-dates-item-text start-date\">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["end_date"] ?? null), "html", null, true));
                    echo "</span></li>
          ";
                }
                // line 189
                echo "           ";
                if ((($context["effort"] ?? null) != "")) {
                    // line 190
                    echo "            <li class=\"important-dates-item\"><span class=\"icon fa fa-pencil\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Estimated Effort</p><span class=\"important-dates-item-text effort\">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["effort"] ?? null), "html", null, true));
                    echo "</span></li>
          ";
                }
                // line 191
                echo "    
         </ol>
      </div>

  
     </div>
      <div class=\"details\">
        <div class=\"inner-wrapper\" id=\"about_data\">   </div>
      </div>




</div>

  <div style=\"display:none;\">
    <form id=\"class_enroll_form\" method=\"post\" data-remote=\"true\" action=\"/change_enrollment\">
       <fieldset class=\"enroll_fieldset\">
         <legend class=\"sr\">Enroll</legend>
         <input name=\"course_id\" type=\"hidden\" value=\"";
                // line 210
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["course_link"] ?? null), "html", null, true));
                echo "\">
         <input name=\"enrollment_action\" type=\"hidden\" value=\"enroll\">
       </fieldset>
       <div class=\"submit\">
        <input name=\"submit\" type=\"submit\" value=\"enroll\">
       </div>
    </form>
  </div>

<div id=\"video-modal\" class=\"modal video-modal\">
<div class=\"inner-wrapper\">
  ";
                // line 221
                if ((($context["video"] ?? null) != "")) {
                    // line 222
                    echo "    <iframe title=\"YouTube Video\" src=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_replace_filter(($context["video"] ?? null), array("watch?v=" => "embed/")), "html", null, true));
                    echo "\" allowfullscreen=\"\" width=\"560\" height=\"315\" frameborder=\"0\">
     </iframe>
   ";
                    // line 227
                    echo " ";
                }
                // line 228
                echo "</div>
</div>

<div id=\"lean_overlay\"></div>    
</div>
</section>
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 236
        echo "
";
    }

    public function getTemplateName()
    {
        return "themes/custom/iitb/templates/views-view-fields--course_about--page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  357 => 236,  344 => 228,  341 => 227,  335 => 222,  333 => 221,  319 => 210,  298 => 191,  292 => 190,  289 => 189,  283 => 187,  280 => 186,  274 => 184,  272 => 183,  269 => 182,  263 => 179,  256 => 175,  240 => 166,  232 => 161,  222 => 156,  214 => 150,  201 => 136,  194 => 132,  190 => 131,  186 => 130,  181 => 127,  170 => 122,  165 => 119,  154 => 114,  150 => 112,  148 => 111,  137 => 103,  133 => 102,  129 => 101,  125 => 100,  120 => 99,  118 => 98,  115 => 97,  111 => 95,  109 => 94,  105 => 91,  103 => 90,  99 => 87,  97 => 86,  93 => 83,  91 => 82,  87 => 79,  85 => 78,  81 => 75,  79 => 74,  75 => 71,  73 => 70,  70 => 67,  66 => 66,  62 => 63,  60 => 62,  56 => 59,  54 => 58,  52 => 57,  48 => 55,  46 => 54,  43 => 53,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/iitb/templates/views-view-fields--course_about--page.html.twig", "/var/www/html/iitbx/themes/custom/iitb/templates/views-view-fields--course_about--page.html.twig");
    }
}
